//
//  ViewController.swift
//  AudioPlayer
//
//  Created by Muzahidul Islam on 6/16/15.
//  Copyright (c) 2015 Digi-TechBD. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController,AVAudioPlayerDelegate,UITableViewDataSource,UITableViewDelegate {
    var player = AVAudioPlayer()
    var playList = [String]()
    var downList = [String]()
    var currentAudioUrl: NSURL!
    var timer: NSTimer!
    var currentAudioIndex = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalAudioLength: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var next: UIButton!
    @IBOutlet weak var playPause: UIButton!
    @IBOutlet weak var previuos: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  First you need to create your audio url
        downList = [
            "http://digi-techbd.com/music/001.mp3",
            "http://digi-techbd.com/music/14%20The%20Lonely%20Shepherd.mp3",
            "http://freetone.org/ring/stan/iPhone_5-Alarm.mp3","http://freetone.org/files/ringtones/0/tonka-dont_be_afraid_mix.mp3",
            "http://freetone.org/files/ringtones/0/vendetta-the_resistance.mp3",
            "http://freetone.org/files/ringtones/0/sergey_lazarev-7_cifr.mp3",
            "http://freetone.org/files/ringtones/0/glyukoza-zachemremix.mp3",
            "http://freetone.org/files/ringtones/0/alena_vasileva-tolko_s_toboy.mp3"
        ]
        
        for link in downList {
            var destinationUrl: NSURL = documentDirectoryURL(link)
            // to check if it exists before downloading it
            if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
                println("The file already exists at path")
            playList.append(link)
                
            } else {
            // if the file doesn't exist
              println("Not download yet")
            }
        }
        if !playList.isEmpty{
            prepareAudio()
        }
        
       
    }

    
   // MARK:- UITableView Datasource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return downList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let identifier = "Cell"
        var cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.textLabel?.text = "\(downList[indexPath.row])"
        
        var destinationUrl: NSURL = documentDirectoryURL(downList[indexPath.row])
        // to check if it exists before downloading it
        if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
         cell.textLabel?.textColor = UIColor.whiteColor()
        }else{
             cell.textLabel?.textColor = UIColor.lightGrayColor()
        }
        return cell
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    // MARK:- UITableView Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
        var destinationUrl: NSURL = documentDirectoryURL(downList[indexPath.row])
        // to check if it exists before downloading it
        if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
          //  println("The file already exists at path")
            if let i = find(playList, downList[indexPath.row]){
                currentAudioIndex = i
                prepareAudio()
                play()
            }
            
        } else {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            dispatch_async(dispatch_get_main_queue()) {
            self.download(self.downList[indexPath.row], completion: { (var urlStr) -> Void in
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    self.playList.append(urlStr)
                    self.currentAudioIndex = self.playList.count-1
                    self.prepareAudio()
                    self.play()
                    self.tableView.reloadData()
                    
                })
            }
            
            
        }
        
        
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        player.currentTime = NSTimeInterval(sender.value)
    }
    
    @IBAction func nextAction(sender: AnyObject) {
        
        if playList.isEmpty{
           ShowAlert("Warning!", message: "Play list is empty, please download first")
            return
        }
        
        currentAudioIndex++
        if currentAudioIndex > playList.count-1{
            currentAudioIndex--
            
            return
        }
        if player.playing{
            prepareAudio()
            play()
        }else{
            prepareAudio()
        }
        
    }
    
    func ShowAlert(title: String,message: String){
        let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    @IBAction func previousAction(sender: AnyObject) {
        
        if playList.isEmpty{
            ShowAlert("Warning!", message: "Play list is empty, please download first")
            return
        }
        currentAudioIndex--
        if currentAudioIndex<0{
            currentAudioIndex++
            return
        }
        if player.playing{
            prepareAudio()
            play()
        }else{
            prepareAudio()
        }

    }
    
    @IBAction func playPauseAction(sender: AnyObject) {
        
        if playList.isEmpty{
            ShowAlert("Warning!", message: "Play list is empty, please download first")
            return
        }
        
        let playImage  = UIImage(named: "play")
        let pauseImage = UIImage(named: "pause")
        
        if player.playing {
            pause()
            playPause.setImage(playImage, forState: UIControlState.Normal)
            
        }else{
            play()
            playPause.setImage(pauseImage, forState: UIControlState.Normal)
        }
        
    }
    
    func startTimer(){
        if timer == nil {
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer:"), userInfo: nil,repeats: true)
            timer.fire()
        }
    }
    
    func stopTimer(){
        timer.invalidate()
        
    }
    
    func updateTimer(timer: NSTimer){
        if !player.playing{
            return
        }
        
        var hour_   = abs(Int(player.currentTime)/3600)
        var minute_ = abs(Int((player.currentTime/60) % 60))
        var second_ = abs(Int(player.currentTime  % 60))
        
        var hour = hour_ > 9 ? "\(hour_)" : "0\(hour_)"
        var minute = minute_ > 9 ? "\(minute_)" : "0\(minute_)"
        var second = second_ > 9 ? "\(second_)" : "0\(second_)"
        
        progressLabel.text  = "\(hour):\(minute):\(second)"
        slider.value = CFloat(player.currentTime)
        
    }

    
    func play(){
        let pauseImage = UIImage(named: "pause")
        playPause.setImage(pauseImage, forState: UIControlState.Normal)
        player.play()
        startTimer()
    }
    
    func pause(){
        let playImage  = UIImage(named: "play")
        playPause.setImage(playImage, forState: UIControlState.Normal)
        player.pause()
    }
    
    func prepareAudio(){
        
        AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        AVAudioSession.sharedInstance().setActive(true, error: nil)
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        
        setCurrentAudioPath()
      
        player = AVAudioPlayer(contentsOfURL: currentAudioUrl, error: nil)
        player.delegate = self
        slider.maximumValue = CFloat(player.duration)
        slider.minimumValue = 0.0
        slider.value = 0.0
        player.prepareToPlay()
        showTotalAudioLenth()

    }
    
    
    // MARK:- AVAudioPlayer Delegate
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool){
        if flag{
            currentAudioIndex++
            if currentAudioIndex > playList.count-1{
                currentAudioIndex--
                return
            }
            prepareAudio()
            play()
        }
    }

    
    
    func showTotalAudioLenth(){
        totalAudioLength.text = calculateTotalLength()
    }
    
    func calculateTotalLength()->String{
        var hour_ = abs(Int(player.duration/3600))
        var minute_ = abs(Int((player.duration/60) % 60))
        var second_ = abs(Int(player.duration % 60))
        
        var hour = hour_ > 9 ? "\(hour_)" : "0\(hour_)"
        var minute = minute_ > 9 ? "\(minute_)" : "0\(minute_)"
        var second = second_ > 9 ? "\(second_)" : "0\(second_)"
        return "\(hour):\(minute):\(second)"
    }
    
    // set current audio
    func setCurrentAudioPath(){
             currentAudioUrl = documentDirectoryURL(playList[currentAudioIndex])
        
    }
    
    // download audio file
    func download(urlStr: String,completion:(urlStr:String)->Void){
        var downloadURL = NSURL(string: urlStr)
        var destinationUrl: NSURL = documentDirectoryURL(urlStr)
            // to check if it exists before downloading it
            if NSFileManager().fileExistsAtPath(destinationUrl.path!) {
              //  println("The file already exists at path")
              //  player = AVAudioPlayer(contentsOfURL: destinationUrl, error: nil)
                // if the file doesn't exist
            } else {
                
                if let myAudioDataFromUrl = NSData(contentsOfURL: downloadURL!){
                    // after downloading your data you need to save it to your destination url
                    if myAudioDataFromUrl.writeToURL(destinationUrl, atomically: true) {
                        completion(urlStr: urlStr)
                       
                    } else {
                        println("error saving file")
                    }
                }
            }

    }
    
    // create document directory
    func documentDirectoryURL(urlStr: String)->NSURL{
        
        var destinationUrl: NSURL!
        if let audioUrl = NSURL(string: urlStr ) {
            
            // then lets create your document folder url
            let documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first as! NSURL
            
            // lets create your destination file url
             destinationUrl = documentsUrl.URLByAppendingPathComponent(audioUrl.lastPathComponent!)
            println(destinationUrl)
        }
        return destinationUrl
    }
    
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

